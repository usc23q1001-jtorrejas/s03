try:
    year = int(input("Please input a year\n"))
    if year > 0:
        if year % 4 == 0:
            if year % 100 == 0:
                if year % 400 == 0:
                    print(f"{year} is a leap year")
                else:
                    print(f"{year} is not a leap year")
            else:
                print(f"{year} is a leap year")
        else:
            print(f"{year} is not a leap year")
    else:
        print("Year must be greater than 0")
except:
    print("Only unsigned numbers are allowed")

row = int(input("Enter number of rows\n"))
column = int(input("Enter number of columns\n"))
for x in range(row):
    for y in range(column):
        print("*", end = ""),
    print("\n")